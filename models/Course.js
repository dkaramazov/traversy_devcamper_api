const mongoose = require('mongoose');

const CourseSchema = mongoose.Schema(
  {
    title: {
      type: String,
      trim: true,
      required: [true, 'Please add a course title.']
    },
    description: {
      type: String,
      required: [true, 'Please add a description']
    },
    weeks: {
      type: String,
      required: [true, 'Please add a number of weeks.']
    },
    tuition: {
      type: Number,
      required: []
    },
    minimumSkill: {
      type: String,
      required: [true, 'Please add a minimum skill'],
      enum: ['beginner', 'intermediate', 'advanced']
    },
    scholarshipAvailable: {
      type: Boolean,
      default: false,
      createAt: {
        type: Date,
        default: Date.now
      }
    },
    bootcamp: {
      type: mongoose.Schema.ObjectId,
      ref: 'Bootcamp',
      required: true
    },
    user: {
      type: mongoose.Schema.ObjectId,
      ref: 'User',
      required: true
    }
  },
  { timestamps: true }
);

CourseSchema.statics.getAverageCost = async function(bootcampId) {
  const arr = await this.aggregate([
    {
      $match: { bootcamp: bootcampId }
    },
    {
      $group: {
        _id: '$bootcamp',
        averageCost: { $avg: '$tuition' }
      }
    }
  ]);
  try {
    const avg = Math.ceil(arr[0].averageCost / 10) * 10;
    await this.model('Bootcamp').findByIdAndUpdate(bootcampId, {
      averageCost: arr[0].averageCost
    });
  } catch (error) {
    throw Error(error);
  }
};

CourseSchema.post('save', function() {
  this.constructor.getAverageCost(this.bootcamp);
});
CourseSchema.post('remove', function() {
  this.constructor.getAverageCost(this.bootcamp);
});

module.exports = mongoose.model('Course', CourseSchema);
