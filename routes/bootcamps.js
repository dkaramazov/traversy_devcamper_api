const {
  getOne,
  getAll,
  create,
  update,
  remove,
  getInRadius,
  uploadPhoto
} = require('../controllers/bootcamps');

const advancedResults = require('../middleware/advancedResults');
const Bootcamp = require('../models/Bootcamp');

const router = require('express').Router();
const { protect, authorize } = require('../middleware/auth');

router.use('/:bootcampId/courses', require('./courses'));
router.use('/:bootcampId/reviews', require('./reviews'));

router.route('/:zipcode/:distance').get(getInRadius);

router
  .route('/')
  .get(advancedResults(Bootcamp, 'courses'), getAll)
  .post(protect, authorize('publisher', 'admin'), create);

router.route('/:id/photo').put(protect, uploadPhoto);

router
  .route('/:id')
  .get(getOne)
  .put(protect, authorize('publisher', 'admin'), update)
  .delete(protect, authorize('publisher', 'admin'), remove);

module.exports = router;
