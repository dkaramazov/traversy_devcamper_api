const { create, getAll, remove, get, update } = require('../controllers/users');
const express = require('express');

const User = require('../models/User');
const { protect, authorize } = require('../middleware/auth');

const advancedResults = require('../middleware/advancedResults');

const router = express.Router();

router.use(protect);
router.use(authorize('admin'));

router
  .post('/create', create)
  .put('/update', update)
  .get('/', advancedResults(User), getAll);

router.get('/:id', get).delete('/:id', remove);

module.exports = router;
