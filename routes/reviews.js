const {
  getAll,
  getOne,
  create,
  update,
  remove
} = require('../controllers/reviews');

const Review = require('../models/Review');
const advancedResults = require('../middleware/advancedResults');

const router = require('express').Router({ mergeParams: true });
const { protect, authorize } = require('../middleware/auth');

router
  .route('/')
  .get(
    advancedResults(Review, { path: 'bootcamp', select: 'name description' }),
    getAll
  )
  .post(protect, authorize('user', 'admin'), create);

router
  .route('/:id')
  .get(getOne)
  .put(protect, authorize('user', 'admin'), update)
  .delete(protect, authorize('user', 'admin'), remove);

module.exports = router;
