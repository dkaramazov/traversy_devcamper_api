const {
  getAll,
  getOne,
  create,
  update,
  remove
} = require('../controllers/courses');

const Course = require('../models/Course');
const advancedResults = require('../middleware/advancedResults');

const router = require('express').Router({ mergeParams: true });
const { protect, authorize } = require('../middleware/auth');

router
  .route('/')
  .get(
    advancedResults(Course, { path: 'bootcamp', select: 'name description' }),
    getAll
  )
  .post(protect, authorize('publisher', 'admin'), create);

router
  .route('/:id')
  .get(getOne)
  .put(protect, authorize('publisher', 'admin'), update)
  .delete(protect, authorize('publisher', 'admin'), remove);

module.exports = router;
