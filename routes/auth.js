const {
  register,
  login,
  logout,
  loggedInUser,
  forgot,
  reset,
  update,
  updatePassword
} = require('../controllers/auth');
const { protect } = require('../middleware/auth');
const express = require('express');

const router = express.Router();

router.post('/register', register);
router.post('/login', login);
router.get('/logout', logout);
router.get('/me', protect, loggedInUser);
router.post('/forgot', forgot);
router.put('/update', protect, update);
router.put('/updatePassword', protect, updatePassword);
router.put('/reset/:reset', reset);

module.exports = router;
