const ErrorResponse = require('../utils/errorResponse');
const AsyncHandler = require('../middleware/async');
const User = require('../models/User');

// @desc get all users
// @route GET /api/v1/auth/users
// @access Private/Admin
exports.getAll = AsyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc get a user
// @route GET /api/v1/auth/users
// @access Private/Admin
exports.get = AsyncHandler(async (req, res, next) => {
  const user = await User.findById(req.params.id);

  res.status(200).json({
    success: true,
    data: user
  });
});

// @desc create a user
// @route POST /api/v1/auth/users
// @access Private/Admin
exports.create = AsyncHandler(async (req, res, next) => {
  const user = await User.create(req.body);

  res.status(201).json({
    success: true,
    data: user
  });
});

// @desc update a user
// @route POST /api/v1/auth/users
// @access Private/Admin
exports.update = AsyncHandler(async (req, res, next) => {
  const user = await User.findByIdAndUpdate(req.params.id, req.body, {
    runValidators: true,
    new: true
  });

  res.status(200).json({
    success: true,
    data: user
  });
});

// @desc delete a user
// @route DELETE /api/v1/auth/users
// @access Private/Admin
exports.remove = AsyncHandler(async (req, res, next) => {
  await User.findByIdAndDelete(req.params.id);

  res.status(200).json({
    success: true,
    data: {}
  });
});
