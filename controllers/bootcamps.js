const path = require('path');
const ErrorResponse = require('../utils/errorResponse');
const AsyncHandler = require('../middleware/async');
const Bootcamp = require('../models/Bootcamp');
const geocoder = require('../utils/geocoder');

// @desc get all
// @route /api/v1/bootcamps
// @access Public
exports.getAll = AsyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});
// @desc create one
// @route /api/v1/bootcamps/:id
// @access Private
exports.create = AsyncHandler(async (req, res, next) => {
  req.body.user = req.user;

  // DON'T ALLOW USER TO CREATE MORE THAN ONE BOOTCAMP IF NOT ADMIN
  const published = await Bootcamp.findOne({ user: req.user.id });

  if (published && req.user.role !== 'admin') {
    throw new ErrorResponse(
      `User has already published a bootcamp profile`,
      400
    );
  }

  const bootcamp = await Bootcamp.create(req.body);
  if (!bootcamp) {
    throw new ErrorResponse('Failed to create Bootcamp', 400);
  }
  res.status(201).json({
    success: true,
    data: bootcamp
  });
});
// @desc get one
// @route /api/v1/bootcamps/:id
// @access Public
exports.getOne = AsyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findById(req.params.id);
  if (!bootcamp) {
    throw new ErrorResponse(
      `Bootcamp not found with an id of ${req.params.id}`,
      404
    );
  }
  res.status(200).json({ success: true, data: bootcamp });
});
// @desc update
// @route /api/v1/bootcamps/:id
// @access Public
exports.update = AsyncHandler(async (req, res, next) => {
  let bootcamp = await Bootcamp.findById(req.params.id);
  if (!bootcamp) {
    throw new ErrorResponse(
      `Bootcamp not found with an id of ${req.params.id}`,
      404
    );
  }

  if (bootcamp.user.toString() !== req.user.id && req.user.role !== 'admin') {
    throw new ErrorResponse(
      `User ${req.user.id} is not authorized to update this bootcamp`,
      400
    );
  }

  bootcamp = await Bootcamp.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  res.status(200).json({ success: true, data: bootcamp });
});
// @desc delete
// @route /api/v1/bootcamps/:id
// @access Private
exports.remove = AsyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findById(req.params.id);
  if (!bootcamp) {
    throw new Error('Not found');
  }

  if (bootcamp.user.toString() !== req.user.id && req.user.role !== 'admin') {
    throw new ErrorResponse(
      `User ${req.user.id} is not authorized to update this bootcamp`,
      400
    );
  }

  bootcamp.remove();
  res.status(200).json({ success: true, data: bootcamp });
});
// @desc get
// @route /api/v1/bootcamps/radius/:zipcode/:distance
// @access Private
exports.getInRadius = AsyncHandler(async (req, res, next) => {
  const { zipcode, distance } = req.params;
  const loc = await geocoder.geocode(zipcode);
  const lat = loc[0].latitude;
  const long = loc[0].longitude;

  const radius = distance / 3963;
  const bootcamps = await Bootcamp.find({
    location: { $geoWithin: { $centerSphere: [[long, lat], radius] } }
  });
  res
    .status(200)
    .json({ success: true, count: bootcamps.length, data: bootcamps });
});

// @desc upload photo
// @route /api/v1/bootcamps/:id
// @access Private
exports.uploadPhoto = AsyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findById(req.params.id);
  if (!bootcamp) {
    throw new ErrorResponse(`No bootcamp found with id of ${req.params.id}`);
  }
  if (!req.files) {
    throw new ErrorResponse(`Please upload an image`);
  }
  const file = req.files.file;
  if (!file.mimetype.startsWith('image')) {
    throw new ErrorResponse(`Please upload a file`, 404);
  }
  if (file.size > process.env.MAX_FILE_UPLOAD) {
    throw new ErrorResponse(`File size too large`, 400);
  }
  file.name = `photo_${bootcamp._id}${path.parse(file.name).ext}`;
  file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async function(
    error
  ) {
    if (error) {
      throw new ErrorResponse(`Failed to save image`, 500);
    }
    await Bootcamp.findByIdAndUpdate(req.params.id, { photo: file.name });
    res.status(200).json({ success: true, filename: file.name });
  });
});
