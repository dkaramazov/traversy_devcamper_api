const ErrorResponse = require('../utils/errorResponse');
const AsyncHandler = require('../middleware/async');
const Course = require('../models/Course');
const Bootcamp = require('../models/Bootcamp');

// @desc get all
// @route /api/v1/courses
// @route /api/v1/bootcamps/:bootcampId
// @access Public
exports.getAll = AsyncHandler(async (req, res, next) => {
  let query;
  if (req.params.bootcampId) {
    const courses = await Course.find({ bootcamp: req.params.bootcampId });
    return res
      .status(200)
      .json({ success: true, count: courses.length, data: courses });
  } else {
    res.status(200).json(res.advancedResults);
  }
});

// @desc get one
// @route /api/v1/courses/:id
// @access Public
exports.getOne = AsyncHandler(async (req, res, next) => {
  const course = await Course.findById(req.params.id).populate({
    path: 'bootcamp',
    select: 'name description'
  });

  if (!course) {
    throw new ErrorResponse(
      `No course found with an id of ${req.params.id}`,
      404
    );
  }
  res.status(200).json({
    success: true,
    data: course
  });
});

// @desc update
// @route /api/v1/courses/:id
// @access Public
exports.update = AsyncHandler(async (req, res, next) => {
  let course = await Course.findById(req.params.id);

  if (!course) {
    throw new ErrorResponse(
      `No course found with an id of ${req.params.id}`,
      404
    );
  }

  if (course.user.toString() !== req.user.id && req.user.role !== 'admin') {
    throw new ErrorResponse(
      `User ${req.user.id} is not authorized to add a course to this bootcamp`,
      400
    );
  }

  course = await Course.findByIdAndUpdate(req.params.id, req.body, {
    runValidators: true,
    new: true
  });
  res.status(200).json({
    success: true,
    data: course
  });
});

// @desc delete
// @route /api/v1/courses/:id
// @access Public
exports.remove = AsyncHandler(async (req, res, next) => {
  let course = await Course.findById(req.params.id);

  if (!course) {
    throw new ErrorResponse(
      `No course found with an id of ${req.params.id}`,
      404
    );
  }

  if (course.user.toString() !== req.user.id && req.user.role !== 'admin') {
    throw new ErrorResponse(
      `User ${req.user.id} is not authorized to add a course to this bootcamp`,
      400
    );
  }

  await Course.remove(course);

  res.status(200).json({
    success: true,
    data: {}
  });
});

// @desc create course
// @route /api/v1/bootcamps/:bootcampId/courses
// @access Public
exports.create = AsyncHandler(async (req, res, next) => {
  req.body.bootcamp = req.params.bootcampId;
  req.body.user = req.user.id;

  const bootcamp = await Bootcamp.findById(req.params.bootcampId);
  if (!bootcamp) {
    throw new ErrorResponse(`No bootcamp with an id of ${req.params.id}`, 404);
  }

  if (bootcamp.user.toString() !== req.user.id && req.user.role !== 'admin') {
    throw new ErrorResponse(
      `User ${req.user.id} is not authorized to add a course to this bootcamp`,
      400
    );
  }

  const course = await Course.create(req.body);

  res.status(200).json({
    success: true,
    data: course
  });
});
