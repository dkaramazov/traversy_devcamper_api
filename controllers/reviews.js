const ErrorResponse = require('../utils/errorResponse');
const AsyncHandler = require('../middleware/async');
const Review = require('../models/Review');
const Bootcamp = require('../models/Bootcamp');

// @desc get all
// @route /api/v1/reviews
// @route /api/v1/bootcamps/:bootcampId/reviews
// @access Public
exports.getAll = AsyncHandler(async (req, res, next) => {
  let query;
  if (req.params.bootcampId) {
    const reviews = await Review.find({ bootcamp: req.params.bootcampId });
    return res
      .status(200)
      .json({ success: true, count: reviews.length, data: reviews });
  } else {
    res.status(200).json(res.advancedResults);
  }
});

// @desc get one
// @route /api/v1/review/:id
// @access Public
exports.getOne = AsyncHandler(async (req, res, next) => {
  const review = await Review.findById(req.params.id).populate({
    path: 'bootcamp',
    select: 'name description'
  });

  if (!review) {
    throw new ErrorResponse(`No review found for ${req.params.id}`, 404);
  }

  res.status(200).json({
    success: true,
    data: review
  });
});

// @desc update
// @route /api/v1/reviews/:id
// @access Public
exports.update = AsyncHandler(async (req, res, next) => {
  let review = await Review.findById(req.params.id);

  if (!review) {
    throw new ErrorResponse(
      `No review found with an id of ${req.params.id}`,
      404
    );
  }

  if (review.user.toString() !== req.user.id && req.user.role !== 'admin') {
    throw new ErrorResponse(
      `User ${req.user.id} is not authorized to add a review to this bootcamp`,
      400
    );
  }

  course = await Review.findByIdAndUpdate(req.params.id, req.body, {
    runValidators: true,
    new: true
  });
  res.status(200).json({
    success: true,
    data: course
  });
});

// @desc delete
// @route /api/v1/reviews/:id
// @access Public
exports.remove = AsyncHandler(async (req, res, next) => {
  let review = await Review.findById(req.params.id);

  if (!review) {
    throw new ErrorResponse(
      `No review found with an id of ${req.params.id}`,
      404
    );
  }

  if (review.user.toString() !== req.user.id && req.user.role !== 'admin') {
    throw new ErrorResponse(
      `User ${req.user.id} is not authorized to add a review to this bootcamp`,
      400
    );
  }

  await review.remove(review);

  res.status(200).json({
    success: true,
    data: {}
  });
});

// @desc create review
// @route /api/v1/bootcamps/:bootcampId/reviews
// @access Public
exports.create = AsyncHandler(async (req, res, next) => {
  req.body.bootcamp = req.params.bootcampId;
  req.body.user = req.user.id;

  const bootcamp = await Bootcamp.findById(req.params.bootcampId);
  if (!bootcamp) {
    throw new ErrorResponse(
      `No bootcamp with an id of ${req.params.bootcampId}`,
      404
    );
  }

  const review = await Review.create(req.body);

  res.status(200).json({
    success: true,
    data: review
  });
});
