const ErrorResponse = require('../utils/errorResponse');
const AsyncHandler = require('../middleware/async');
const User = require('../models/User');
const sendEmail = require('../utils/sendEmail');
const crypto = require('crypto');

// @desc register
// @route POST /api/v1/auth/register
// @access Public
exports.register = AsyncHandler(async (req, res, next) => {
  const { name, email, password, role } = req.body;

  const user = await User.create({
    name,
    email,
    password,
    role
  });

  sendTokenResponse(user, 200, res);
});

// @desc login
// @route POST /api/v1/auth/login
// @access Public
exports.login = AsyncHandler(async (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    throw new ErrorResponse(`Please provide email and password`, 400);
  }

  const user = await User.findOne({ email }).select('+password');

  if (!user) {
    throw new ErrorResponse(`Invalid credentials`, 401);
  }

  const match = await user.matchPassword(password);

  if (!match) {
    throw new ErrorResponse(`Invalid credentials`, 401);
  }

  sendTokenResponse(user, 200, res);
});

const sendTokenResponse = async (user, statusCode, res) => {
  const token = user.getSignedJWT();
  const options = {
    expires: new Date(
      Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000
    ),
    httpOnly: true
  };

  if (process.env.NODE_ENV === 'production') {
    options.secure = true;
  }

  res
    .status(statusCode)
    .cookie('token', token, options)
    .json({ success: true, token });
};

// @desc get logged in user
// @route GET /api/v1/auth/logout
// @access Private
exports.logout = AsyncHandler(async (req, res, next) => {
  res.cookie('token', 'none', {
    expires: new Date(Date.now() + 10 * 1000),
    httpOnly: true
  });

  res.status(200).json({ success: true, data: {} });
});

// @desc get logout
// @route GET /api/v1/auth/me
// @access Private
exports.loggedInUser = AsyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user.id);
  res.status(200).json({ success: true, data: user });
});
// @desc update password
// @route PUT /api/v1/auth/updatePassword
// @access Private
exports.updatePassword = AsyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user.id).select('+password');

  if (!(await user.matchPassword(req.body.currentPassword))) {
    throw new ErrorResponse('Password is incorrect', 401);
  }

  user.password = req.body.newPassword;
  await user.save();

  sendTokenResponse(user, 200, res);
});

// @desc update user details
// @route PUT /api/v1/auth/update
// @access Private
exports.update = AsyncHandler(async (req, res, next) => {
  const user = await User.findByIdAndUpdate(
    req.user.id,
    { name: req.body.name, email: req.body.email },
    {
      new: true,
      runValidators: true
    }
  );
  res.status(200).json({ success: true, data: user });
});

// @desc reset password
// @route PUT /api/v1/auth/reset
// @access Public
exports.reset = AsyncHandler(async (req, res, next) => {
  const token = crypto
    .createHash('sha256')
    .update(req.params.reset)
    .digest('hex');

  const user = await User.findOne({
    resetPasswordToken: token,
    resetPasswordExpire: { $gt: Date.now() }
  });

  if (!user) {
    throw new ErrorResponse('User not found', 400);
  }

  user.resetPasswordToken = undefined;
  user.resetPasswordExpire = undefined;
  await user.save();

  sendTokenResponse(user, 200, res);
});

// @desc forgot password
// @route GET /api/v1/auth/forgot
// @access Public
exports.forgot = AsyncHandler(async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });

  if (!user) {
    throw new ErrorResponse(`No user found`, 404);
  }

  const resetToken = user.getResetPasswordToken();

  await user.save({ validateBeforeSave: false });

  // create reset url
  const resetURL = `${req.protocol}://${req.get(
    'host'
  )}/api/v1/auth/reset/${resetToken}`;

  const message = `You have received this to reset password!! use: ${resetURL}`;

  try {
    await sendEmail({
      email: user.email,
      subject: 'Password reset request',
      message
    });
    res.status(200).json({
      success: true,
      data: 'Email sent'
    });
  } catch (error) {
    console.log(error);
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;
    await user.save({ validateBeforeSave: false });
    throw new ErrorResponse('Email could not be sent', 500);
  }

  res.status(200).json({ success: true, data: user });
});
