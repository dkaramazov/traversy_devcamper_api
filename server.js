const express = require('express');
const morgan = require('morgan');
const connectDB = require('./config/db');
const fileUpload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const errorHandler = require('./middleware/error');
const mongoSanitize = require('express-mongo-sanitize');
const helmet = require('helmet');
const xssClean = require('xss-clean');
const hpp = require('hpp');
const rateLimit = require('express-rate-limit');
const cors = require('cors');
const path = require('path');
// load env variables
require('dotenv').config({ path: './config/config.env' });

// connect to DB
connectDB();

const app = express();

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}
// middleware
app.use(express.json());
app.use(fileUpload());
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

app.use(errorHandler);
app.use(mongoSanitize());
app.use(helmet());
app.use(xssClean());

const limiter = rateLimit({
  windowMs: 10 * 60 * 1000, // 15 minutes
  max: 100 // limit each IP to 100 requests per windowMs
});
app.use(limiter);

app.use(hpp());
app.use(cors());

// load up routes
app.use('/api/v1/bootcamps', require('./routes/bootcamps'));
app.use('/api/v1/courses', require('./routes/courses'));
app.use('/api/v1/auth', require('./routes/auth'));
app.use('/api/v1/users', require('./routes/users'));
app.use('/api/v1/reviews', require('./routes/reviews'));

const PORT = 5000 || process.env.PORT;

const server = app.listen(
  PORT,
  console.log(`server running on port ${PORT} in ${process.env.NODE_ENV} mode`)
);

process.on('unhandledRejection', (err, promise) => {
  console.log('Global Error: ', err.message);
  server.close(() => process.exit(1));
});
