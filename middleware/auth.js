const jwt = require('jsonwebtoken');
const AsyncHandler = require('./async');
const ErrorResponse = require('../utils/errorResponse');
const User = require('../models/User');

exports.protect = AsyncHandler(async (req, res, next) => {
  let token;
  const { authorization } = req.headers;
  if (authorization && authorization.startsWith('Bearer')) {
    token = authorization.split(' ')[1];
  }
  // else if (req.cookies && req.cookies.token) {
  //   token = req.cookies.token;
  // }
  if (!token) {
    throw new ErrorResponse(`Not authorized`, 401);
  }

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user = await User.findOne({ id: decoded._id });
    next();
  } catch (error) {
    throw new ErrorResponse(`Not authorized`, error, 401);
  }
});

exports.authorize = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      throw new ErrorResponse(`Role ${req.user.role} not authorized.`, 403);
    }
    next();
  };
};
